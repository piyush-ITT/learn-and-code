#include<iostream>
#include <stack>
using namespace std;
bool testcaseConstraint(int);
bool friendsConstraint(int, int);
bool friendPopularityConstraint(int);
void removeChristieFriends(int, int);
void popFriend();
void christieFriendsAfterRemoval();
int removeFriendsFromFriendlist;
stack<int> friendsPopularity;
int main()
{
	int testcase, currentFriends, friendPopularity;
	cin>>testcase;
	if(!testcaseConstraint(testcase))
	{
		return 0;
	}
	for(int testcaseCount = 0; testcaseCount < testcase; testcaseCount++)
	{
		cin>>currentFriends>>removeFriendsFromFriendlist>>friendPopularity;
		if(!friendsConstraint(currentFriends, removeFriendsFromFriendlist))
		{
			return 0;
		}
		if(!friendPopularityConstraint(friendPopularity))
		{
			return 0;
		}
		removeChristieFriends(currentFriends, friendPopularity);
		while (removeFriendsFromFriendlist > 0) 
		{
			popFriend();
		}
		christieFriendsAfterRemoval();
	}
	return 0;
}

bool testcaseConstraint(int testcase)
{
	const int minimumTestcaseValue = 1;
	const int maximumTestcaseValue = 1000;  
	if(testcase >= minimumTestcaseValue && testcase <= maximumTestcaseValue)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool friendsConstraint(int currentFriends, int removeFriendsFromFriendlist)
{
	const int minimumCurrentFriendsValue = 1;
	const int maximumCurrentFriendsValue = 100000;
	const int minimumFriendsCanRemove = 0;
	const int maximumFriendsCanRemove = 100000;
	if(currentFriends >= minimumCurrentFriendsValue && currentFriends <= maximumCurrentFriendsValue && removeFriendsFromFriendlist >= minimumFriendsCanRemove && removeFriendsFromFriendlist < currentFriends)
	{
		return true;
	} 
	else
	{
		return false;
	}
}

inline bool friendPopularityConstraint(int friendPopularity)
{
	const int minimumFriendPopularity = 0;
	const int maximumFriendPopularity = 100;
	if(friendPopularity >= 0 && friendPopularity <= maximumFriendPopularity)
	{
		return true;
	} 
	else
	{
		return false;
	}
}

void removeChristieFriends(int currentFriends, int friendPopularity)
{
	friendsPopularity.push(friendPopularity);
	for (int popularityCount = 1; popularityCount < currentFriends; popularityCount++) 
	{
		cin>>friendPopularity;
		if(!friendPopularityConstraint(friendPopularity))
		{
			exit(0);
		}
		while (!friendsPopularity.empty() && friendsPopularity.top() < friendPopularity && removeFriendsFromFriendlist > 0) 
		{
			popFriend();
		}
		friendsPopularity.push(friendPopularity);
	}
}

inline void popFriend() 
{
	friendsPopularity.pop();
	removeFriendsFromFriendlist--;
}

void christieFriendsAfterRemoval()
{
	stack<int> friendList;
	while(!friendsPopularity.empty())
	{
		friendList.push(friendsPopularity.top());
		friendsPopularity.pop();
	}
	
	while(!friendList.empty())
	{
		cout<<friendList.top()<<" ";
		friendList.pop();
	}
	cout<<endl;
}

