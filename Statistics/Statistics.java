import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Statistics {
	private LinkedHashMap<String, Integer> sportsList = new LinkedHashMap<String, Integer>();
	private int numberOfPeople;
	private static Scanner sc;

	public static void main(String[] args) {
		Statistics statistics = new Statistics();
		sc = new Scanner(System.in);
		statistics.numberOfPeople = sc.nextInt();
		if (!statistics.numberOfPeopleConstraint(statistics.numberOfPeople)) {
			return;
		}
		for (int peopleCount = 0; peopleCount < statistics.numberOfPeople; peopleCount++) {
			String personName = sc.next();
			String game = sc.next();
			if (!statistics.checkLengthOfPersonNameAndItsFavouriteGame(personName, game)) {
				return;
			}
			int likedCount = 1;
			statistics.addPeopleFavouriteGame(game, likedCount);
		}
		statistics.showMostLikedSports();
		// displaying count of people whose favourite game is football
		System.out.println(statistics.sportsList.get("football") == null ? "0" : statistics.sportsList.get("football"));
	}

	private void addPeopleFavouriteGame(String game, int likedCount) {
		if (sportsList.containsKey(game)) {
			// update count of people by 1
			sportsList.put(game, sportsList.get(game) + 1);
		} else {
			// storing game with initial like count
			sportsList.put(game, likedCount);
		}
	}

	private void showMostLikedSports() {
		Integer maximumLikedSportCount = Collections.max(sportsList.values());
		for (Map.Entry entry : sportsList.entrySet()) {
			if (maximumLikedSportCount.equals(entry.getValue())) {
				System.out.println(entry.getKey());
				break;
			}
		}
	}

	private boolean numberOfPeopleConstraint(int numberOfPeople) {
		final int minimumNumberOfPeople = 1, maximumNumberOfPeople = 100000;
		if (numberOfPeople >= minimumNumberOfPeople && numberOfPeople <= maximumNumberOfPeople) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkLengthOfPersonNameAndItsFavouriteGame(String personName, String game) {
		final int minimumLengthOfPersonName = 1, maximumLengthOfPersonName = 10;
		final int minimumNameOfGameLength = 1, maximumNameOfGameLength = 10;
		int personNameLength = personName.length();
		int nameOfGameLength = game.length();
		if (personNameLength >= minimumLengthOfPersonName && personNameLength <= maximumLengthOfPersonName
				&& nameOfGameLength >= minimumNameOfGameLength && nameOfGameLength <= maximumNameOfGameLength) {
			return true;
		} else {
			return false;
		}
	}

}
