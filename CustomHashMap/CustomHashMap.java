import java.util.ArrayList;
import java.util.LinkedList;

class HashNode<K, V> {
	K key;
	V value;
	int hashCode;

	public HashNode(K key, V value) {
		this.key = key;
		this.value = value;
		hashCode = key.hashCode();
	}
}

public class CustomHashMap<K, V> {
	K key;
	V value;
	private int bucketCapacity;
	private ArrayList<LinkedList<HashNode<K, V>>> hashBucket = new ArrayList<LinkedList<HashNode<K, V>>>();

	public CustomHashMap(int bucketCapacity) {
		for (int index = 0; index < bucketCapacity; index++) {
			hashBucket.add(null);
		}
		this.bucketCapacity = bucketCapacity;
	}

	/**
	 * Function to store the value with its key
	 * @param key
	 * @param value
	 */
	public void put(K key, V value) {
		int currentHashCode = generateHashCode(key);
		int hashIndex = currentHashCode % bucketCapacity;
		if (hashBucket.get(hashIndex) != null) {
			LinkedList<HashNode<K, V>> existingHashLinkedList = hashBucket.get(hashIndex);
			existingHashLinkedList.add(new HashNode<K, V>(key, value));
		} else {
			LinkedList<HashNode<K, V>> hashLinkedList = new LinkedList<HashNode<K, V>>();
			hashLinkedList.add(new HashNode<K, V>(key, value));
			hashBucket.set(hashIndex, hashLinkedList);
		}
	}

	/**
	 * Function to get value corresponding to it's key
	 * @param key
	 * @return
	 */
	public V get(K key) {
		int currentHashCode = generateHashCode(key);
		int hashIndex = currentHashCode % bucketCapacity;
		if (hashBucket.get(hashIndex) != null) {
			LinkedList<HashNode<K, V>> hashLinkedList = hashBucket.get(hashIndex);
			for (HashNode<K, V> hashnode : hashLinkedList) {
				if (hashnode.key.equals(key)) {
					return hashnode.value;
				}
			}
		} else {
			return null;
		}
		return null;
	}

	/**
	 * Function to display all the hash table values with their keys
	 */
	public void displayAll() {
		for (LinkedList<HashNode<K, V>> hashList : hashBucket) {
			if (hashList == null)
				continue;
			for (HashNode<K, V> currentNode : hashList) {
				System.out.println("<" + currentNode.key + "," + currentNode.value + ">");
			}
		}
	}
	/**
	 * Function to generate hash code
	 * @param key
	 * @return
	 */
	public int generateHashCode(K key) {
		int keyHashCode = 0;
		String sKey = key.toString();
		int keyLength = sKey.length();
		for(int index = 1; index <= keyLength; index++) {
			int keyAsciiValue = (int)sKey.charAt(index-1);
			keyHashCode = keyHashCode + (index*keyAsciiValue);
		}
		return keyHashCode;
	}
}
