import java.util.Scanner;

public class HashMapImplementation {
	private static Scanner sc;
	private static CustomHashMap<String, String> customHashMap;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		int choice;
		System.out.println("Please enter bucket capacity");
		customHashMap = new CustomHashMap<String, String>(sc.nextInt());
		do {
			// Menu which will show to user initially
			System.out.println("Welcome enter the following operation you want to perform:-");
			System.out.println("Enter 0 for Exit");
			System.out.println("1. Insert");
			System.out.println("2. Retrieve");
			System.out.println("3. Display");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Please enter key & value");
				customHashMap.put(sc.next(), sc.next());
				break;
			case 2:
				System.out.println("Please enter key");
				System.out.println(customHashMap.get(sc.next()));
				break;
			case 3:
				customHashMap.displayAll();
				break;
			case 0:
				choice = 0;
				break;
			default:
				System.out.println("-----------invalid input-----------\n");

			}
		} while (choice != 0);
	}
}
