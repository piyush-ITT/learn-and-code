import java.util.ArrayList;
import java.util.Scanner;

class MonkAndChamberOfSecrets
{
    	private static Scanner sc = new Scanner(System.in);
    	private int numberOfSpidersInQueue;
	private int selectedSpiders;
	private ArrayList<Integer> powerOfSpiders = new ArrayList<Integer>();
	private int spiderMaxPowerIndex;
	private int spiderMaxPower;
	private int indexValueOfArrayList;
	public static void main(String []args)
	{	
		MonkAndChamberOfSecrets monkAndChamberOfSecrets = new MonkAndChamberOfSecrets();		
		monkAndChamberOfSecrets.numberOfSpidersInQueue = sc.nextInt();
		monkAndChamberOfSecrets.selectedSpiders = sc.nextInt();
		if(!monkAndChamberOfSecrets.validateConsraintsForSelection(monkAndChamberOfSecrets.numberOfSpidersInQueue, monkAndChamberOfSecrets.selectedSpiders))
		{
			return;
		}
		for(int spiderPowerCounter = 1; spiderPowerCounter <= monkAndChamberOfSecrets.numberOfSpidersInQueue; spiderPowerCounter++)
		{	
			int powerOfSpider = sc.nextInt();
			if(!monkAndChamberOfSecrets.validateConsraintForPower(powerOfSpider, monkAndChamberOfSecrets.selectedSpiders))
			{
				return;
			}
			monkAndChamberOfSecrets.powerOfSpiders.add(powerOfSpider);
		}
		monkAndChamberOfSecrets.monkHelp();	
	}

	private void monkHelp() 
	{
		int outputCounter = 0, selectionCount = 1;
		indexValueOfArrayList = 1;
		for(int selectedSpidersCounter = 1; selectedSpidersCounter <= selectedSpiders; selectedSpidersCounter++)
		{
			indexValueOfArrayList = incrementIfNegativeValue(indexValueOfArrayList);		    
			spiderMaxPowerIndex = indexValueOfArrayList;
			spiderMaxPower = powerOfSpiders.get(indexValueOfArrayList-1);
			powerAfterDecrement();
			findMaxPowerIndex(selectionCount);
			selectionCount = settingValueOfSelectionCount(selectionCount, outputCounter);		
			System.out.print(spiderMaxPowerIndex + " ");
			checkArraySizeAndIncrement();
			powerOfSpiders.set(spiderMaxPowerIndex - 1, -1);
			outputCounter++;
		}	
	}

	private void findMaxPowerIndex(int selectionCount) 
	{
		while(selectionCount < selectedSpiders)
		{		
			if(indexValueOfArrayList < powerOfSpiders.size())	
			{		
				indexValueOfArrayList = incrementIfNegativeValue(indexValueOfArrayList + 1) - 1;
				maxPowerLogic();				
			}
			else
			{
					indexValueOfArrayList = indexValueOfArrayList % powerOfSpiders.size();
					indexValueOfArrayList = incrementIfNegativeValue(indexValueOfArrayList + 1) - 1;
					maxPowerLogic();
			}
			indexValueOfArrayList++;
			selectionCount++;
			powerAfterDecrement();
			
		}
	}

	private void maxPowerLogic() 
	{
		if(spiderMaxPower < powerOfSpiders.get(indexValueOfArrayList) )
		{
			spiderMaxPower = powerOfSpiders.get(indexValueOfArrayList);
			spiderMaxPowerIndex = indexValueOfArrayList + 1;					
		}
	}

	private void powerAfterDecrement() 
	{
		int powerAfterDecrement = powerOfSpiders.get(indexValueOfArrayList - 1) - 1;
		if(powerAfterDecrement >= 0)
		{
			powerOfSpiders.set(indexValueOfArrayList - 1, powerAfterDecrement);
		}
	}

	private int incrementIfNegativeValue(int indexValueOfArrayList) 
	{
		while(powerOfSpiders.get(indexValueOfArrayList - 1) == -1)
		{
			indexValueOfArrayList = checkArraySizeAndIncrement();
		}
		return indexValueOfArrayList;
	}
	
	private int settingValueOfSelectionCount(int selectionCount, int outputCounter) 
	{
		if((powerOfSpiders.size() - outputCounter) > selectedSpiders )
		{
			selectionCount = 1;
		}
		else
		{
			if(selectedSpiders == powerOfSpiders.size())
			{
				selectionCount = outputCounter + 2;
			}
			else
			{
				selectionCount = outputCounter + 1;
			}
		}
		return selectionCount;
	}

	private int checkArraySizeAndIncrement() 
	{
		if(indexValueOfArrayList == powerOfSpiders.size())
		{
			indexValueOfArrayList = 1;
		}
		else
		{
			indexValueOfArrayList++;
		}
		return indexValueOfArrayList;
	}
	
	private boolean validateConsraintsForSelection(int numberOfSpidersInQueue, int selectedSpiders) 
	{
		final int selectedSpidersLowerBound = 1;
		final int selectedSpidersUpperBound = 316;
		final int numberOfSpidersInQueueLowerBound = selectedSpiders;
		final int numberOfSpidersInQueueUpperBound = selectedSpiders * selectedSpiders;
		if(selectedSpiders >= selectedSpidersLowerBound && selectedSpiders <= selectedSpidersUpperBound && numberOfSpidersInQueue>= numberOfSpidersInQueueLowerBound && numberOfSpidersInQueue <= numberOfSpidersInQueueUpperBound)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private boolean validateConsraintForPower(int powerOfSpider, int selectedSpiders) 
	{
		final int powerOfSpiderLowerBound = 1;
		final int powerOfSpiderUpperBound = selectedSpiders;
		if(powerOfSpider >= powerOfSpiderLowerBound && powerOfSpider <= powerOfSpiderUpperBound)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}