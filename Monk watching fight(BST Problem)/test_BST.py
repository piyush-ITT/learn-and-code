import unittest
from BST import BST


class TestBST(unittest.TestCase):
    def __init__(self, *args):
        super(TestBST, self).__init__(*args)
        self.bst = BST(10)
        self.bst.insert(self.bst, 20)
        self.bst.insert(self.bst, 50)
        self.bst.insert(self.bst, 30)
        self.bst.insert(self.bst, 5)
        self.bst.insert(self.bst, 40)
        self.bst.insert(self.bst, 1)

    def test_search(self):
        self.assertTrue(self.bst.search(self.bst, 5))
        self.assertTrue(self.bst.search(self.bst, 50))
        self.assertTrue(self.bst.search(self.bst, 20))
        self.assertFalse(self.bst.search(self.bst, 80))
        self.assertTrue(self.bst.search(self.bst, 40))

    def test_height(self):
        self.assertEqual(self.bst.height(self.bst), 5)


if __name__ == '__main__':
    unittest.main()
