import sys

MIN_NUMBER_OF_ELEMENTS = MIN_VALUE_OF_LIST_OF_ELEMENTS = 1
MAX_NUMBER_OF_ELEMENTS = 10 ** 3
MAX_VALUE_OF_LIST_OF_ELEMENTS = 10 ** 6


class BST:
    def __init__(self, value):
        self.data = value
        self.left = None
        self.right = None

    def insert(self, node, value):
        if node.data < value:
            if node.right is None:
                node.right = BST(value)
            else:
                self.insert(node.right, value)
        else:
            if node.left is None:
                node.left = BST(value)
            else:
                self.insert(node.left, value)

    def height(self, node):
        if node is None:
            return 0
        left_depth = self.height(node.left)
        right_depth = self.height(node.right)
        if left_depth > right_depth:
            return left_depth + 1
        else:
            return right_depth + 1


if __name__ == "__main__":
    number_of_elements = int(raw_input())
    if not (MIN_NUMBER_OF_ELEMENTS <= number_of_elements <= MAX_NUMBER_OF_ELEMENTS):
        sys.exit("Number of elements in list should be in between 1 to 1000")
    list_of_elements = map(int, raw_input().split())
    if not (all(MIN_VALUE_OF_LIST_OF_ELEMENTS <= value <= MAX_VALUE_OF_LIST_OF_ELEMENTS for value in list_of_elements)):
        sys.exit("value in list of elements should be in between 1 to 1000000")
    value_equality_flag = False
    for index in range(number_of_elements - 1):
        if list_of_elements[index] == list_of_elements[index + 1]:
            value_equality_flag = True
        else:
            value_equality_flag = False
            break
    if not value_equality_flag:
        root = BST(list_of_elements[0])
        for index in range(1, number_of_elements):
            root.insert(root, list_of_elements[index])
        height_of_BST = root.height(root)
        print height_of_BST
    else:
        print number_of_elements
