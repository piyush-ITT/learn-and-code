package com.Test;

import java.util.Scanner;
import java.util.Stack;


class Fest 
{
    private static Scanner s;

	public static void main(String args[] ) throws Exception 
    {
        Stack <Integer> idStore = new Stack <Integer>();
        s = new Scanner(System.in);
        int testCase = s.nextInt();
        int i=0;
        while (i < testCase) 
        {
        	// checking if stack is not empty delete all element 
            while(!idStore.isEmpty())
            {
                idStore.pop();
            }
            // taking input of no. of passes and current player id
            int numberOfPasses = s.nextInt();
            int currentPlayerId = s.nextInt();
            if (!validate(testCase, numberOfPasses, currentPlayerId))   // Checking constraints
            {
                return;
            }
            idStore.push(currentPlayerId);
            int k=0;
            while (k < numberOfPasses)
            {
                String pass = s.next();
                if ("b".equalsIgnoreCase(pass))
                {
                    int idTop = idStore.peek();
                    idStore.pop();
                    int idPrv = idStore.peek();
                    idStore.push(idTop);
                    idStore.push(idPrv);
                    ++k;
                }
                else
                {
	                int currentPlayerId1 = s.nextInt();
	                if (!validate(testCase, numberOfPasses, currentPlayerId1)) 
	                {
	                    return;
	                }
	                idStore.push(currentPlayerId1);
	                ++k;
                }
            }
            // Printing output which player has ball in last
            System.out.println("Player " + idStore.peek());
            i++;
        }

    }
	/*
	 *  Method checking constraints 
	 */
	private static boolean validate(int testCase, int numberOfPasses, int currentPlayerId) 
	{
		if(testCase >= 0 && testCase <= 100 && numberOfPasses >= 1 && numberOfPasses <= 100000 && currentPlayerId >= 1 && currentPlayerId <= 1000000)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
